package com.clockworkant.recipes

import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface RecipePuppyApi {
    @GET("api/")
    fun getRecipes(@Query("q") searchTerm: String): Single<RecipePuppyResponse>
}