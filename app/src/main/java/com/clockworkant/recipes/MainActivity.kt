package com.clockworkant.recipes

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.SearchView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.partial_recipe_item.view.*

class MainActivity : AppCompatActivity() {

    private val recipeRepo = RecipeRepo(RecipePuppyApiImpl())
    private val recipeAdapter = RecipeAdapter()
    private val compositeSubscription = CompositeDisposable()
    private var queryTerm = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initRecyclerView()
        initSearchView()
    }

    private fun initSearchView() {
        recipelist_searchview.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean = true

            override fun onQueryTextChange(newText: String?): Boolean {
                queryTerm = newText!!
                fetchAndDisplayQuery(queryTerm)
                return true
            }
        })
    }

    private fun fetchAndDisplayQuery(queryTerm: String) {
        compositeSubscription.clear()
        compositeSubscription.add(
                recipeRepo.getRecipes(queryTerm)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                {
                                    recipes ->
                                    recipeAdapter.updateItems(recipes)
                                },
                                {
                                    error ->
                                    recipeAdapter.updateItems(emptyList())
                                    Log.d("RecipesPrototype", "Get Recipes error", error)
                                }
                        )
        )
    }

    override fun onPause() {
        super.onPause()
        compositeSubscription.clear()
    }

    override fun onResume() {
        super.onResume()
        fetchAndDisplayQuery(queryTerm)
    }

    private fun initRecyclerView() {
        recipelist_recyclerview.setHasFixedSize(true)
        recipelist_recyclerview.layoutManager = LinearLayoutManager(this)
        recipelist_recyclerview.adapter = recipeAdapter
    }

    private class RecipeAdapter : RecyclerView.Adapter<RecipeViewHolder>() {
        var items: List<Recipe> = emptyList()

        override fun onBindViewHolder(holder: RecipeViewHolder?, position: Int) {
            holder?.bind(items[position])
        }

        override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): RecipeViewHolder {
            val v = LayoutInflater.from(parent!!.context).inflate(R.layout.partial_recipe_item, parent, false)
            return RecipeViewHolder(v)
        }

        override fun getItemCount(): Int = items.size

        fun updateItems(newItems: List<Recipe>) {
            items = newItems
            notifyDataSetChanged()
        }
    }

    private class RecipeViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView) {
        fun bind(recipe: Recipe) = with(itemView) {
            itemView.recipeitem_name.text = recipe.name
        }
    }
}
