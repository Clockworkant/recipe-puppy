package com.clockworkant.recipes

internal data class Recipe(val name: String)