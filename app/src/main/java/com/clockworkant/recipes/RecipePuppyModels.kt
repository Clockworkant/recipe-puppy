package com.clockworkant.recipes

class RecipePuppyResponse(val results: List<RecipePuppyResult>)

class RecipePuppyResult(
        val title: String,
        val href: String,
        val ingredients: String,
        val thumbnail: String
)