package com.clockworkant.recipes

import io.reactivex.Single
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class RecipePuppyApiImpl : RecipePuppyApi {

    private var api: RecipePuppyApi

    init {
        val retrofit = Retrofit.Builder()
                .baseUrl("http://www.recipepuppy.com")
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()

        api = retrofit.create(RecipePuppyApi::class.java)
    }

    override fun getRecipes(searchTerm: String): Single<RecipePuppyResponse> {
        return api.getRecipes(searchTerm)
    }

}