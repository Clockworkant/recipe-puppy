package com.clockworkant.recipes

import io.reactivex.Single

internal class RecipeRepo(val api: RecipePuppyApi) {

    fun getRecipes(queryText: String): Single<List<Recipe>> {

        return api.getRecipes(queryText)
                .map {
                    it.results.map {
                        Recipe(it.title)
                    }
                }
    }
}