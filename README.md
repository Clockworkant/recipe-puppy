# Details
The demo project is written in kotlin and uses the sythentic properties
### Time
Spent 2 hours on the project. 

### Unfulfilled requirements
* the requirements specify 20 results but the api returns 10. Paging still needs to be written an encapsulated within a manager

### Issues encountered
* learning about searchview
* first time using retrofit with kotlin (used with java normally)

### Other
* would usually package by feature but we only have 1 feature in this case
